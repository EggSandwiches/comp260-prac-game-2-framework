﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle2 : MonoBehaviour {

    private Rigidbody rigidbody;
    public float speed = 5f;
    public Rigidbody target;
    
    

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        // Follow the Puck
        Vector3 direction = target.position - transform.position;
        direction = direction.normalized;
        rigidbody.velocity = direction * speed;
    }

    
}
